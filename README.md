# stupid-api
Completely dumb and stupidly simple json data api.
Has no db or anything only files.

## Install
> composer update
> composer dump-autoload

## Run
> php -S 127.0.0.1:8080 -t ./public

## Usage
Retrieve data
> GET https://127.0.0.1:8080/example

Update data with a json body in the request
> POST https://127.0.0.1:8080/example

Create new item
> POST https://127.0.0.1:8080/cat