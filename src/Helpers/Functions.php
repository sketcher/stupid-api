<?php

namespace StupidApi\Helpers;

use StupidApi\Models\Path;

class Functions
{

    static function json($item, $success = true)
    {
        ob_start();
        if ($success) {
            http_response_code(200);
        }
        header('Content-Type: application/json');
        $jsonString = json_encode($item, JSON_UNESCAPED_UNICODE);
        echo $jsonString;
        register_shutdown_function(function() {
            header('Content-Length: ' . ob_get_length());
            ob_end_flush();
            die();
        });
    }

    static function bad_request()
    {
        Functions::error(400, 'Bad Request');
    }

    static function method_not_allowed()
    {
        Functions::error(405, 'Method Not Allowed');
    }

    static function error(int $errorCode, string $message)
    {
        http_response_code($errorCode);
        Functions::json(['error' => $message], false);
        exit;
    }

    static function trimds($s)
    {
        return rtrim($s, DIRECTORY_SEPARATOR);
    }

    static function join_paths(string ...$paths)
    {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    static function json_file_exists(string $path): bool
    {
        return file_exists($path);
    }

    static function read_json(string $path)
    {
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        return $jsonData;
    }

    /**
     * With list asterisk(*), display all files + directories
     */
    static function extract_fileinfos_in_path(Path $path): array
    {
        $files = scandir($path->GetFullDirectoryPath());
        $files = array_values(array_map(function ($str) use ($path) {
            return preg_match('/\.json$/', $str) ? substr($str, 0, -5) : $str;
        }, array_filter($files, fn ($file) => $file !== '.' && $file !== '..')));
        return $files;
    }

    // static function full_file_path(string $path)
    // {
    //     $path = Functions::join_paths(Config::getInstance()::$data_folder, $path) . '.json';
    //     return $path;
    // }

    static function get_full_url(): string
    {
        $protocol_https = filter_input(INPUT_SERVER, 'HTTPS', FILTER_SANITIZE_STRING);
        $host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL);
        $request_uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
        if (strlen($request_uri) == 0) {
            $request_uri = filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_SANITIZE_URL);
            $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_URL);
            if ($query_string) {
                $request_uri .= '?' . $query_string;
            }
        }
        $full_url = ($protocol_https ? 'https' : 'http') . '://' . $host . $request_uri;
        return $full_url;
    }

    static function allow_cors()
    {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, post, get');
        // header("Access-Control-Max-Age", "3600");
        header('Access-Control-Allow-Headers: *');
        header("Access-Control-Allow-Origin: *");
    }

    static function merge_recursively($arr1, $arr2)
    {
        $keys = array_keys($arr2);
        foreach ($keys as $key) {
            if (
                isset($arr1[$key])
                && is_array($arr1[$key])
                && is_array($arr2[$key])
            ) {
                $arr1[$key] = Functions::merge_recursively($arr1[$key], $arr2[$key]);
            } else {
                $arr1[$key] = $arr2[$key];
            }
        }
        return $arr1;
    }
}
