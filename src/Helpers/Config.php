<?php
namespace StupidApi\Helpers;
class Config
{
    public static string $root_url = "http://localhost:8080/";
    public static string $root_path_replacement_pattern = "/^\/api\//i";
    public static string $data_folder;

    private static $instance = null;

    // The constructor is private
    // to prevent initiation with outer code.
    private function __construct()
    {
        self::$data_folder = realpath(Functions::join_paths(__DIR__, '..', '..', 'data'));
    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Config();
        }

        return self::$instance;
    }
}
