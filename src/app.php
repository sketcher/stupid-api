<?php

namespace StupidApi;


use StupidApi\Helpers\Functions;
use StupidApi\Helpers\Config;
use StupidApi\Models\JSONFile;
use StupidApi\Models\Method;
use StupidApi\Models\Path;
use StupidApi\Models\Request;

$request = new Request();
$path = new Path($request);

#region initial validations and returns
if (!$path->IsAcceptableUrl())
    Functions::error(404, '404 - Invalid URL');
if(!$request->validateUrlCharacters())
    Functions::error(400, 'Invalid URL characters. Only [a-z0-9_] are allowed');
#endregion

$controller = new Controller($request, $path, Config::getInstance());

switch ($request->method) {
    case Method::GET:
        $controller->GetJson();
        break;
    case  Method::POST:
        $controller->PostJson();
        break;
    case Method::DELETE:
        $controller->DeleteJson();
        break;
    case Method::OPTIONS:
        Functions::allow_cors();
        break;
    default:
        Functions::method_not_allowed();
        break;
}
