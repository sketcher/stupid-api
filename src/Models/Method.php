<?php

namespace StupidApi\Models;

enum Method: string
{
    case GET = 'GET';
    case HEAD = 'HEAD';
    case POST = 'POST';
    case PUT = 'PUT';
    case DELETE = 'DELETE';
    case CONNECT = 'CONNECT';
    case OPTIONS = 'OPTIONS';
    case TRACE = 'TRACE';
    case PATCH = 'PATCH';
    case OTHER = 'OTHER';
    
    public static function fromName(string $name): Method
    {
        $method = self::tryFromName($name);
        return $method === null ? $enumName = static::class : $method;
    }

    public static function tryFromName(?string $name): ?Method
    {
        if (is_null($name))
            return null;

        $name = strtoupper($name);

        if (defined("self::$name")) {
            /**
             * @var Method
             */
            $enumCase = constant("self::$name");
            return $enumCase;
        }

        return null;
    }
}
