<?php

namespace StupidApi\Models;

use StupidApi\Helpers\Config;
use StupidApi\Helpers\Functions;
use StupidApi\Models\Method;

interface IRequest
{
    public function getMethod(): Method;
    public function getHeaders(): array;
    public function getUrl(): string;
    public function getPathAsParts(): array;
    public function isAsteriskRequest(): bool;
    public function validateUrlCharacters(): bool;
}

class Request implements IRequest
{
    public Method $method;
    public array $headers;
    public array $parts = [];
    public string $url;

    public function __construct()
    {
        $this->method = Method::tryFromName($_SERVER['REQUEST_METHOD']);
        $this->url = Functions::get_full_url();

        $this->headers = getallheaders() ?? [];
    }

    public function getMethod(): Method
    {
        return $this->method;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function isAsteriskRequest(): bool
    {
        $parts = $this->getPathAsParts();
        return end($parts) === '*';
    }

    public function validateUrlCharacters(): bool
    {
        $parts = $this->getPathAsParts();
        $isGood = true;;
        foreach ($parts as $index => $part) {
            if (!preg_match('/^[a-z0-9_\-]+$/', $part)) {
                if ($index + 1 >= count($parts)) {}
                else {
                    $isGood = false;
                }
            }
        }
        return $isGood;
    }

    public function getPathAsParts(): array
    {
        if ($this->parts !== []) {
            return $this->parts;
        }
        $parts = [];
        ['path' => $rawPath] = parse_url($this->url);
        $rawPath = preg_replace(Config::getInstance()::$root_path_replacement_pattern, '/', $rawPath);

        foreach (explode('/', $rawPath) as $part) {
            $part = rtrim($part, '/');
            if ($part !== '')
                $parts[] = $part;
        }
        $this->parts = $parts;
        return $this->parts;
    }
}
