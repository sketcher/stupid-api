<?php
namespace StupidApi\Models;

use StupidApi\Helpers\Config;
use StupidApi\Helpers\Functions;

class JSONFile
{
    private string $filepath; // With .json file extension
    
    public function __construct($filepath)
    {
        $this->filepath = $filepath;
    }

    private function PathWithoutFilename(): string
    {
        return substr($this->filepath, 0, -5); // '.json' = 5
    }

    public function exists(): bool
    {
        return $this->filepath !== null && file_exists($this->filepath);
    }

    public function read(): array
    {
        if (!$this->exists()) {
            return [];
        }
        $jsonString = file_get_contents($this->filepath);
        $jsonData = json_decode($jsonString, true);
        return $jsonData;
    }

    public function update($data)
    {
        $oldData = $this->read();
        // $newData = array_merge_recursive($oldData, $data);
        $newData = Functions::merge_recursively( $oldData, $data );
        $this->write($newData);
    }

    public function write(array $data): void
    {
        if(!is_dir($this->PathWithoutFilename())) {
            mkdir($this->PathWithoutFilename(), 0777, true);
        }
        file_put_contents($this->filepath, json_encode($data), LOCK_EX);
    }

    public function delete(): bool
    {
        return unlink($this->filepath);
    }
}
