<?php

namespace StupidApi\Models;

use StupidApi\Helpers\Config;
use StupidApi\Helpers\Functions;

interface IPath
{
    public function GetFullDirectoryPath(...$args): string;
    public function GetFullJsonPath(): string;
    public function IsDirectory(): bool;
    public function IsJsonFile(): bool;
    public function IsAcceptableUrl(): bool;
}

class Path implements IPath
{
    public array $parts = [];
    public string $rawPath = '';
    public IRequest $request;
    public function __construct(IRequest $request)
    {
        $this->request = $request;
        ['path' => $rawPath] = parse_url($this->request->getUrl());
        $rawPath = preg_replace(Config::getInstance()::$root_path_replacement_pattern, '/', $rawPath);
        $this->rawPath = $rawPath;
        $this->parts = $this->request->getPathAsParts();
    }

    public function GetFullDirectoryPath(...$args): string
    {
        $arr = $this->parts;
        if ($this->request->isAsteriskRequest()) {
            array_pop($arr);
        }
        $path = Functions::join_paths(Config::getInstance()::$data_folder, ...$arr, ...$args);
        return $path;
    }

    public function GetFullJsonPath(): string
    {
        $arr = $this->parts;
        $arr[count($arr) - 1] .= '.json'; // [0, 1, 2, 3] => [0, 1, 2.3
        $path = Functions::join_paths(Config::getInstance()::$data_folder, ...$arr);
        return $path;
    }

    public function GetUnsanitizedPath(): string
    {
        return $this->rawPath;
    }

    public function IsDirectory(): bool
    {
        $path = $this->GetFullDirectoryPath();
        return is_dir($path);
    }

    public function IsJsonFile(): bool
    {
        $path = $this->GetFullDirectoryPath() . '.json';
        return file_exists($path);
    }

    public function IsAcceptableUrl(): bool
    {
        if ($this->rawPath === '' || $this->rawPath === '/')
            return false;
        return true;
    }
}
