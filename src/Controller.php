<?php

namespace StupidApi;

use StupidApi\Helpers\Config;
use StupidApi\Helpers\Functions;
use StupidApi\Models\JSONFile;
use StupidApi\Models\Path;
use StupidApi\Models\Request;

class Controller
{
    private Request $request;
    private Path $path;
    private Config $config;
    public function __construct(Request $request, Path $path, Config $config)
    {
        $this->request = $request;
        $this->path = $path;
        $this->config = $config;
    }

    public function GetJson()
    {
        Functions::allow_cors();

        $path = $this->path;

        if ($this->request->isAsteriskRequest()) {

            if (!$this->path->IsDirectory())
                Functions::error(403, 'Not a directory, cant list');
            $files = Functions::extract_fileinfos_in_path($path);
            Functions::json($files);
        } else if ($path->IsJsonFile()) {

            $file = new JSONFile($path->GetFullJsonPath());
            if (!$file->exists()) {
                Functions::error(404, $path->rawPath . ' does not exist');
            }
            Functions::json($file->read());
        } else {
            Functions::error(404, $path->rawPath . ' does not exist');
        }
    }

    public function PostJson()
    {
        Functions::allow_cors();
        $path = $this->path;

        $data = json_decode(file_get_contents('php://input'), true);
        if ($data === null)
            Functions::bad_request();


        $file = new JSONFile($path->GetFullJsonPath());
        if (!$path->IsJsonFile()) {
            $file->write([]);
        }
        $file->update($data);
        Functions::json($file->read());
    }

    public function DeleteJson(){
        Functions::allow_cors();
        $path = $this->path;
        $file = new JSONFile($path->GetFullJsonPath());
        if (!$file->exists()) {
            Functions::error(404, $path->GetUnsanitizedPath() . ' does not exist');
        }
        $file->delete();
    }
}
